package com.techdatahub.batch

import java.sql.Date
import java.text.SimpleDateFormat

import com.techdatahub.config.BatchProcessorConf
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

class SensorDataBatchProcessor(spark: SparkSession,
                               config: BatchProcessorConf,
                               fileSystemConf: Configuration = new Configuration() /* Needed for the tests */ ,
                               currentMillis: Long = System.currentTimeMillis()) {

  def run(): Unit = {
    val dataFiles = getDataFiles(config.inputDataDir)

    val fileDFs = getFileDataFrames(dataFiles)
    val unionDF: DataFrame = fileDFs.reduce(_ union _)

    val meanAccelerationValuesDF = calcMeanAccelerationValues(unionDF)
    meanAccelerationValuesDF.show()
    writeToCSV(meanAccelerationValuesDF.repartition(1), s"${config.outputDir}/avg_accel_$currentMillis")

    val maxJerkDF = calcMaxJerk(unionDF)
    maxJerkDF.show()
    writeToCSV(maxJerkDF.repartition(1), s"${config.outputDir}/max_jerk_$currentMillis")
  }

  private[batch] def calcMeanAccelerationValues(unionDF: DataFrame): DataFrame = {
    unionDF
      .groupBy("event", "label")
      .agg(
        count("*").as("total_count"),
        avg("accel_x").as("avg_accel_x"),
        avg("accel_y").as("avg_accel_y"),
        avg("accel_z").as("avg_accel_z")
      )
      .orderBy("event", "label")
      .withColumn("total_accel", sqrt(pow(col("avg_accel_x"), 2) + pow(col("avg_accel_y"), 2) + pow(col("avg_accel_z"), 2)))
  }

  /**
   * In order to calculate the Jerk for each row, we need to access the acceleration values from the previous row.
   * At first it seams that this can be achieved with the LAG() window function over a Window specification.
   *
   * In Spark a window can be created by providing both partitionBy and orderBy columns, e.g Window.partitionBy("col1").orderBy("col2")
   * Spark uses the partitionBy to shuffle the data between partitions and then sorts the records
   * within every partition considering the orderBy columns.
   *
   * It's also possible to create a window by just providing the orderBy columns, e.g. Window.orderBy("col2")
   * But in this case all the data will be sent to a single partition, when you do so the Spark even logs the following warning:
   * WARN WindowExec: No Partition Defined for Window operation! Moving all data to a single partition, this can cause
   * serious performance degradation
   *
   * While the second approach will work just fine when processing small datasets, this approach
   * should not be used in production environments to process large datasets, because the data will
   * not fit in a single partition and the Executor will simply crash with OutOfMemory error.
   *
   * Since we need to order the entire dataset, which may be large and we can't use the partitionBy statement, window functions do not solve our problem.
   *
   * As workaround, the dataset DataFrame is joined with itself, to obtain for each row the required values from the previous row.
   *
   */
  private[batch] def calcMaxJerk(df: DataFrame): DataFrame = {
    val leftDF = df.as("df1")
    val rightDF = df.as("df2")
    val joinExpr = col("df1.row") === (col("df2.row") + 1) and col("df1.source_file") === col("df2.source_file")

    val selfJoinedDF = leftDF
      .join(rightDF, joinExpr, "inner")
      .select(
        col("df1.source_file"), col("df1.row"), col("df1.timestamp"), col("df1.accel_x"),
        col("df1.accel_y"), col("df1.accel_z"), col("df2.row").as("prev_row"),
        col("df2.timestamp").as("prev_timestamp"), col("df2.accel_x").as("prev_accel_x"),
        col("df2.accel_y").as("prev_accel_y"), col("df2.accel_z").as("prev_accel_z")
      )

    val calcJerkDF = selfJoinedDF
      .withColumn("timestamp_diff", col("timestamp") - col("prev_timestamp"))
      .withColumn("accel_x_diff", col("accel_x") - col("prev_accel_x"))
      .withColumn("accel_y_diff", col("accel_y") - col("prev_accel_y"))
      .withColumn("accel_z_diff", col("accel_z") - col("prev_accel_z"))
      .withColumn("jerk_accel_x", col("accel_x_diff") / col("timestamp_diff"))
      .withColumn("jerk_accel_y", col("accel_y_diff") / col("timestamp_diff"))
      .withColumn("jerk_accel_z", col("accel_z_diff") / col("timestamp_diff"))
      .withColumn("total_jerk", (abs(col("jerk_accel_x")) + abs(col("jerk_accel_y")) + abs(col("jerk_accel_z"))).cast(DoubleType))
      .select(col("source_file"), col("row"), col("timestamp"), col("total_jerk"))

    calcJerkDF.show()

    val maxJerkValue = calcJerkDF.groupBy().agg(max(col("total_jerk"))).collect()(0).getDouble(0)

    val maxJerkDF = calcJerkDF.where(col("total_jerk") === lit(maxJerkValue))

    // To obtain the time in the required format using the Spark SQL functions is not enough, because the Timestamp type column
    // can only be constructed using unix timestamp in seconds, e.g. date_format(from_unixtime(1549625320), "yyyy-MM-dd HH:mm:ss.SSS")
    // Since we need milliseconds information, we manually determine the formatted time by accessing the underlying RDD.
    val maxJerkWithFormattedTimeRDD = maxJerkDF.rdd.mapPartitions { rowIter =>
      val dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
      rowIter.map { row =>
        val timeMillis = row.getLong(2)
        val date = new Date(timeMillis)
        Row(row(0), row(1), row(2), dateFormat.format(date))
      }
    }

    val schema = StructType(Seq(
      StructField("source_file", StringType),
      StructField("row", LongType),
      StructField("timestamp", LongType),
      StructField("formatted_time", StringType)
    ))

    val maxJerkWithFormattedTimeDF = spark.createDataFrame(maxJerkWithFormattedTimeRDD, schema)
    maxJerkWithFormattedTimeDF
  }

  private[batch] def getDataFiles(inputDataDir: String): Seq[SensorDataFile] = {
    val fs = FileSystem.get(fileSystemConf)
    val status = fs.listStatus(new Path(inputDataDir))
    status.map { x =>
      val filename = x.getPath.getName
      // For the sake of simplicity I'm assuming all the data files have the same naming, e.g. aggressive_bump_1550163148318484.csv
      // This allows me to easily extract the motion event type
      val filenameParts = filename.split("_\\d+\\.csv")
      SensorDataFile(filenameParts(0), filename, x.getPath.toString)
    }
  }

  private[batch] def getFileDataFrames(dataFiles: Seq[SensorDataFile]): Seq[DataFrame] = {
    val schema = StructType(Seq(
      StructField("row", LongType),
      StructField("timestamp", LongType),
      StructField("accel_x", DecimalType(30, 20)),
      StructField("accel_y", DecimalType(30, 20)),
      StructField("accel_z", DecimalType(30, 20)),
      StructField("gyro_roll", DecimalType(30, 20)),
      StructField("gyro_pitch", DecimalType(30, 20)),
      StructField("gyro_yaw", DecimalType(30, 20)),
      StructField("label", StringType)
    ))

    dataFiles.map { file =>
      spark
        .read
        .schema(schema)
        .option("header", "true")
        .csv(file.path)
        .withColumn("timestamp", (col("timestamp") / 1000).cast(LongType)) // Convert timestamp to millis
        .withColumn("event", lit(file.event))
        .withColumn("source_file", lit(file.filename))
    }
  }

  private def writeToCSV(df: DataFrame, destinationDir: String): Unit = {
    df.write
      .option("header", "true")
      .csv(destinationDir)
  }
}