package com.techdatahub.batch

case class SensorDataFile(event: String, filename: String, path: String)