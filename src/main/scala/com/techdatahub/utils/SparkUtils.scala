package com.techdatahub.utils

import com.techdatahub.utils.SparkTargetMode.SparkTargetMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}

object SparkTargetMode extends Enumeration {
  type SparkTargetMode = Value

  val Local = Value
  val Standalone = Value
  val Yarn = Value
}

object SparkUtils extends Loggable {

  def newSparkContext(appName: String,
                      sparkTargetMode: SparkTargetMode = SparkTargetMode.Local,
                      sparkMasterURL: String = "spark://localhost:7077"): SparkContext = {
    if(sparkTargetMode == SparkTargetMode.Local) {
      INFO("Running in Local")
      val sparkConfig = new SparkConf()
      sparkConfig.set("spark.broadcast.compress", "false")
      sparkConfig.set("spark.shuffle.compress", "false")
      sparkConfig.set("spark.shuffle.spill.compress", "false")
      sparkConfig.set("spark.io.compression.codec", "lzf")
      new SparkContext("local[*]", appName, sparkConfig)
    } else if(sparkTargetMode == SparkTargetMode.Standalone) {
      INFO("Running in Standalone")
      new SparkContext(sparkMasterURL, appName, new SparkConf().setAppName(appName))
    } else {
      INFO("Running in Cluster")
      new SparkContext(new SparkConf().setAppName(appName))
    }
  }

  def newSparkSession(appName: String,
                      hiveSupportEnabled: Boolean = false,
                      uiEnabled: Boolean = true,
                      sparkTargetMode: SparkTargetMode = SparkTargetMode.Local,
                      sparkMasterURL: String = "spark://localhost:7077",
                      extraConfigs: Map[String, String] = Map()): SparkSession = {

    val sparkSessionBuilder = SparkSession.builder().appName(appName)

    if(sparkTargetMode == SparkTargetMode.Local) {
      INFO("Running in Local")
      sparkSessionBuilder
        .master("local[*]")
        .config("spark.broadcast.compress", false)
        .config("spark.shuffle.compress", false)
        .config("spark.shuffle.spill.compress", false)
        .config("spark.io.compression.codec", "lzf")
    } else if(sparkTargetMode == SparkTargetMode.Standalone) {
      INFO("Running in Standalone")
      sparkSessionBuilder
        .master(sparkMasterURL)
        .config("spark.broadcast.compress", false)
        .config("spark.shuffle.compress", false)
        .config("spark.shuffle.spill.compress", false)
        .config("spark.io.compression.codec", "lzf")
    } else {
      INFO("Running in Cluster")
      sparkSessionBuilder
        .config(new SparkConf())
    }

    extraConfigs.foreach { case (k, v) => sparkSessionBuilder.config(k, v) }

    if(hiveSupportEnabled) sparkSessionBuilder.enableHiveSupport()
    sparkSessionBuilder.config("spark.ui.enabled", uiEnabled)

    sparkSessionBuilder.getOrCreate()
  }
}