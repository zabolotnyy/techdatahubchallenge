package com.techdatahub.utils

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.io.Source
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

object JsonDeserializer extends Loggable {

  private val mapper = {
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    objectMapper.disable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES)
    objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
    objectMapper.enable(DeserializationFeature.WRAP_EXCEPTIONS)
    objectMapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)
    objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS)
    objectMapper
  }

  def fromClasspathFile[T](filePath: String)(implicit tag: ClassTag[T]): T = {
    INFO(s"Reading JSON from '$filePath'")
    val jsonStr = readFileFromClasspath(filePath)
    fromJson[T](jsonStr)
  }

  def fromJson[T](jsonStr: String)(implicit tag: ClassTag[T]): T = {
    Try(mapper.readValue(jsonStr, tag.runtimeClass)) match {
      case Success(c) => c.asInstanceOf[T]
      case Failure(t) => throw new RuntimeException(s"Couldn't deserialize. Please check if the JSON is valid or if some required fields are not missing", t)
    }
  }

  private def readFileFromClasspath(filePath: String): String = {
    val inputStream = getClass.getClassLoader.getResourceAsStream(filePath)
    Source.fromInputStream(inputStream).mkString
  }
}