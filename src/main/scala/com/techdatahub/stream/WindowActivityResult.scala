package com.techdatahub.stream

import java.sql.Timestamp

case class WindowActivityResult(source: String, window_from: Timestamp, window_to: Timestamp, window_points: Int,
                                avg_accel_x: Double, avg_accel_y: Double, avg_accel_z: Double,
                                breaking: Boolean, acceleration: Boolean, right_turn: Boolean, left_turn: Boolean)
