package com.techdatahub.stream

import com.techdatahub.config.StreamProcessorConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.types._

class SensorDataStreamProcessor(spark: SparkSession, conf: StreamProcessorConf) {

  private var runningQuery: StreamingQuery = _

  def startStream(): Unit = {
    val streamDF = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", conf.kafka.kafkaBootstrapServers)
      .option("subscribe", conf.kafka.kafkaTopic)
      .load()

    // Extracting the key and value fields contained in each fetched Kafka record
    // To define a Window in Structured Streaming, we need to have a column in Dataframe of the type Timestamp.
    val rawRecordsDF = streamDF
      .selectExpr("cast(key as string)", "cast(value as string)")

    val preparedDF = rawRecordsDF
      .withColumn("_tmp", split(col("value"), ","))
      .select(
        col("key").as("source"),
        col("_tmp").getItem(0).cast(LongType).as("row"),
        col("_tmp").getItem(1).cast(LongType).divide(1000000).cast(TimestampType).as("timestamp"), // Because casting to Timestamp requires time in seconds
        col("_tmp").getItem(2).cast(DecimalType(30, 20)).as("accel_x"),
        col("_tmp").getItem(3).cast(DecimalType(30, 20)).as("accel_y"),
        col("_tmp").getItem(4).cast(DecimalType(30, 20)).as("accel_z")
      ).drop("_tmp")

    preparedDF.printSchema()

    val windowedAggregationDF = preparedDF
      // Specify how late the records can arrive considering their generation time
      // In Append output mode (default) Spark only outputs the result from a window after the watermark has passed
      // This means it is completely okay to have a delay until results are written to the Sink
      .withWatermark("timestamp", "30 seconds")
      .groupBy(window(col("timestamp"), "5 seconds"), col("source"))
      .agg(
        count("*").cast(IntegerType).as("window_points"),
        avg("accel_x").cast(DoubleType).as("avg_accel_x"),
        avg("accel_y").cast(DoubleType).as("avg_accel_y"),
        avg("accel_z").cast(DoubleType).as("avg_accel_z")
      )

    val categorizedDF = windowedAggregationDF
      .withColumn("window_from", col("window.start"))
      .withColumn("window_to", col("window.end"))
      .withColumn("breaking", when(col("avg_accel_x").lt(conf.accelerationBounds.breakUpperBound), true).otherwise(false))
      .withColumn("acceleration", when(col("avg_accel_x").gt(conf.accelerationBounds.accelerationLowerBound), true).otherwise(false))
      .withColumn("right_turn", when(col("avg_accel_y").lt(conf.accelerationBounds.rightUpperBound), true).otherwise(false))
      .withColumn("left_turn", when(col("avg_accel_y").gt(conf.accelerationBounds.leftLowerBound), true).otherwise(false))
      .drop("window")

    import spark.implicits._

    val resultDS = categorizedDF.as[WindowActivityResult]

    val jdbcSink: JDBCSink = conf.jdbc.dbType.toLowerCase match {
      case "postgres" => new PostgresJDBCSink(conf.jdbc.url, conf.jdbc.user, conf.jdbc.password, conf.jdbc.outputTable)
      case "oracle" => throw new NotImplementedError("Oracle JDBC Sink is not supported")
      case "mysql" => throw new NotImplementedError("MySQL JDBC Sink is not supported")
    }

    runningQuery = resultDS.writeStream.foreach(jdbcSink).start()

    runningQuery.awaitTermination()
  }

  def endStream(): Unit = {
    if(runningQuery != null) runningQuery.stop()
  }
}