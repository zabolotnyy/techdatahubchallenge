package com.techdatahub.stream

import java.sql.{Connection, DriverManager, PreparedStatement}

import com.techdatahub.utils.Loggable
import org.apache.spark.sql.ForeachWriter

// See https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.ForeachWriter
abstract class JDBCSink(url: String, user: String, password: String, outputTable: String) extends ForeachWriter[WindowActivityResult] with Loggable {

  var connection: Connection = _
  var preparedStatement: PreparedStatement = _

  override def open(partitionId: Long, version: Long): Boolean = {
    loadDriverClass()
    connection = DriverManager.getConnection(url, user, password)
    preparedStatement = connection.prepareStatement(getStatement())
    true
  }

  override def close(errorOrNull: Throwable): Unit = {
    if(preparedStatement != null) preparedStatement.close()
    if(connection != null) connection.close()
    if(errorOrNull != null) ERROR(errorOrNull)("An unexpected error occurred!")
  }

  def loadDriverClass(): Unit

  def getStatement(): String

  def process(value: WindowActivityResult): Unit
}
