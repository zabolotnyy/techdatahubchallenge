package com.techdatahub.stream

class PostgresJDBCSink(url: String, user: String, password: String, outputTable: String)
  extends JDBCSink(url, user, password, outputTable) {

  override def loadDriverClass(): Unit = Class.forName("org.postgresql.Driver")

  override def getStatement(): String =
    s"""INSERT INTO $outputTable (id, source, window_from, window_to, window_points,
      |   avg_accel_x, avg_accel_y, avg_accel_z, breaking, acceleration, right_turn, left_turn)
      |VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
      |""".stripMargin

  override def process(result: WindowActivityResult): Unit = {
    INFO(s"Processing $result ")
    preparedStatement.setString(1, result.source)
    preparedStatement.setTimestamp(2, result.window_from)
    preparedStatement.setTimestamp(3, result.window_to)
    preparedStatement.setInt(4, result.window_points)
    preparedStatement.setDouble(5, result.avg_accel_x)
    preparedStatement.setDouble(6, result.avg_accel_y)
    preparedStatement.setDouble(7, result.avg_accel_z)
    preparedStatement.setBoolean(8, result.breaking)
    preparedStatement.setBoolean(9, result.acceleration)
    preparedStatement.setBoolean(10, result.right_turn)
    preparedStatement.setBoolean(11, result.left_turn)
    preparedStatement.executeUpdate()
  }
}
