package com.techdatahub.config

case class BatchProcessorConf(inputDataDir: String, outputDir: String) {

  def validate(): Unit = {
    // TODO make sure the configured fields are valid
  }
}