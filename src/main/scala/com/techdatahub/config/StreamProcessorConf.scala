package com.techdatahub.config

case class StreamProcessorConf(kafka: KafkaDetails,
                               accelerationBounds: AccelerationBounds,
                               jdbc: JDBCDetails) {
  def validate(): Unit = {
    // TODO make sure the configured fields are valid
  }
}

case class KafkaDetails(kafkaTopic: String, kafkaBootstrapServers: String)

case class AccelerationBounds(breakUpperBound: Double, accelerationLowerBound: Double, rightUpperBound: Double, leftLowerBound: Double)

case class JDBCDetails(dbType: String, url: String, user: String, password: String, outputTable: String)