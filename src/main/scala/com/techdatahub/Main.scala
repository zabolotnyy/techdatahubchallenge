package com.techdatahub

import com.techdatahub.batch.SensorDataBatchProcessor
import com.techdatahub.config.{BatchProcessorConf, StreamProcessorConf}
import com.techdatahub.stream.SensorDataStreamProcessor
import com.techdatahub.utils.{JsonDeserializer, Loggable, SparkTargetMode, SparkUtils}

object Main extends App with Loggable {

  // TODO argument checking / handling
  val spark =
    if(args.isEmpty) {
      throw new IllegalArgumentException("No arguments provided!")
    }
    else if(args.length == 2 && args(1).toLowerCase == "local") {
      SparkUtils.newSparkSession(
        appName = "TechDataHubChallenge",
        sparkTargetMode = SparkTargetMode.Local
      )
    }
    else if(args.length == 4 && args(2).toLowerCase == "standalone") {
      SparkUtils.newSparkSession(
        appName = "TechDataHubChallenge",
        sparkTargetMode = SparkTargetMode.Standalone,
        sparkMasterURL = args(3)
      )
    }
    else {
      // To run in YARN Cluster mode $HADOOP_CONF_DIR/yarn-site.xml should be passed to application's classpath, so that
      // Spark knows the Resource Manager it should connect to
      SparkUtils.newSparkSession(
        appName = "TechDataHubChallenge",
        sparkTargetMode = SparkTargetMode.Yarn
      )
    }

  // Assuming the arguments were previously checked and are correct
  if(args(0).toLowerCase == "batch") {
    val conf = JsonDeserializer.fromClasspathFile[BatchProcessorConf]("batch_config.json")
    conf.validate()
    // Since the $HADOOP_CONF_DIR/core-site.xml will be passed to application's classpath, we don't need to define the fileSystemConf: Configuration argument
    val batchProcessor = new SensorDataBatchProcessor(spark, conf)
    batchProcessor.run()
  }
  else {  // any other value counts as streaming
    val conf = JsonDeserializer.fromClasspathFile[StreamProcessorConf]("stream_config.json")
    conf.validate()
    val streamProcessor = new SensorDataStreamProcessor(spark, conf)
    streamProcessor.startStream()
  }
}