#!/usr/bin/env bash

BASE_DIR=$(dirname "$(pwd)")

CONFIG_DIR=$BASE_DIR/configs
JARS_DIR=$BASE_DIR/jars

# Executable Class and JAR
EXEC_CLASS=com.techdatahub.Main
EXEC_JAR=$JARS_DIR/challenge-1.0.0.jar

# Logs
LOG4J_PROPERTIES=$CONFIG_DIR/log4j.properties

# Java Options
JAVA_OPTS="-Dlog4j.configuration=file://$LOG4J_PROPERTIES"

# Running arguments
RUN_ARGS="stream local"

java -cp $EXEC_JAR:$CONFIG_DIR:$HADOOP_CONFIG_DIR $JAVA_OPTS $EXEC_CLASS $RUN_ARGS