package com.techdatahub.batch

import java.math.BigDecimal

import com.techdatahub.config.BatchProcessorConf
import com.techdatahub.testing.{SparkTestingEnv, UnitSpec}

class SensorDataBatchProcessorSpec extends UnitSpec with SparkTestingEnv {

  before {
    startSpark()
  }

  after {
    shutdownSpark()
  }

  // Example of a Unit test using Spark's local execution
  "SensorDataBatchProcessor" should "properly calculate max Jerk" in {
    val batchProcessor = new SensorDataBatchProcessor(spark, BatchProcessorConf("", ""))

    val sparkLocalDecl = spark
    import sparkLocalDecl.implicits._

    val currentMillis = 1565900162235l

    val df = spark.sparkContext.makeRDD(Seq(
      (0l, currentMillis, new BigDecimal("0.09"), new BigDecimal("0.08"), new BigDecimal("0.07"),
        new BigDecimal("0.0"), new BigDecimal("0.0"), new BigDecimal("0.0"), 0, "aggressive_bump", "aggressive_bump_1550163148300000.csv"),
      (0l, currentMillis, new BigDecimal("0.01"), new BigDecimal("0.01"), new BigDecimal("0.01"),
        new BigDecimal("0.0"), new BigDecimal("0.0"), new BigDecimal("0.0"), 0, "aggressive_bump", "aggressive_bump_1550163148311111.csv"),
      (1l, currentMillis + 50, new BigDecimal("0.02"), new BigDecimal("0.01"), new BigDecimal("0.03"),
        new BigDecimal("0.0"), new BigDecimal("0.0"), new BigDecimal("0.0"), 0, "aggressive_bump", "aggressive_bump_1550163148311111.csv"),
      (2l, currentMillis + 100, new BigDecimal("0.01"), new BigDecimal("0.08"), new BigDecimal("0.01"),
        new BigDecimal("0.0"), new BigDecimal("0.0"), new BigDecimal("0.0"), 0, "aggressive_bump", "aggressive_bump_1550163148311111.csv"),
      (3l, currentMillis + 150, new BigDecimal("0.01"), new BigDecimal("0.01"), new BigDecimal("0.01"),
        new BigDecimal("0.0"), new BigDecimal("0.0"), new BigDecimal("0.0"), 0, "aggressive_bump", "aggressive_bump_1550163148311111.csv")
    )).toDF("row", "timestamp", "accel_x", "accel_y", "accel_z", "gyro_roll", "gyro_pitch", "gyro_yaw", "label", "event", "source_file")

    val maxJerkDF = batchProcessor.calcMaxJerk(df)
    maxJerkDF.show()

    val collected = maxJerkDF.collect().toSeq
    INFO(s"Collected: $collected")

    assert(collected.length === 1)
    assert(collected(0).toSeq sameElements Seq("aggressive_bump_1550163148311111.csv", 2, currentMillis + 100, "2019-08-15 21:16:02.335"))
  }
}
