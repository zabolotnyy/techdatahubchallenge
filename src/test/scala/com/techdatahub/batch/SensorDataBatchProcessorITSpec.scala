package com.techdatahub.batch

import com.techdatahub.config.BatchProcessorConf
import com.techdatahub.testing.{SparkTestingEnv, UnitSpec}
import com.techdatahub.utils.JsonDeserializer
import org.apache.hadoop.conf.Configuration

class SensorDataBatchProcessorITSpec extends UnitSpec with SparkTestingEnv {

  before {
    startSpark()
  }

  after {
    shutdownSpark()
  }

  // TODO to automate the integration tests I would use the Maven Failsafe Plugin https://maven.apache.org/surefire/maven-failsafe-plugin/index.html
  // So that the integration tests (those containing "IT" in the class name) are run in their own "integration-test" Maven build phase
  // Additionally I will also use this Maven Plugin https://github.com/fabric8io/docker-maven-plugin to automatically startup the docker containers,
  // run the required initialization scripts and stop the containers in "pre-integration-test" and "post-integration-test" Maven build phases.
  ignore should "correctly run end to end" in {
    val currentMillis = System.currentTimeMillis()

    val conf = JsonDeserializer.fromClasspathFile[BatchProcessorConf]("batch_test_config.json")
    val fsConf = new Configuration()
    fsConf.set("fs.defaultFS", "hdfs://localhost:9000/")
    val batchProcessor = new SensorDataBatchProcessor(spark, conf, fsConf, currentMillis)

    batchProcessor.run()

    // TODO here I would assert that the batch was correctly processed by checking the generated output
  }
}