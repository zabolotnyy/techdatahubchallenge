package com.techdatahub.stream

import com.techdatahub.config.StreamProcessorConf
import com.techdatahub.testing.{SparkTestingEnv, UnitSpec}
import com.techdatahub.utils.JsonDeserializer

class SensorDataStreamProcessorITSpec extends UnitSpec with SparkTestingEnv {

  before {
    startSpark()
  }

  after {
    shutdownSpark()
  }

  // TODO to automate the integration tests I would use the Maven Failsafe Plugin https://maven.apache.org/surefire/maven-failsafe-plugin/index.html
  // So that the integration tests (those containing "IT" in the class name) are run in their own "integration-test" Maven build phase
  // Additionally I will also use this Maven Plugin https://github.com/fabric8io/docker-maven-plugin to automatically startup the docker containers,
  // run the required initialization scripts and stop the containers in "pre-integration-test" and "post-integration-test" Maven build phases.
  ignore should "correctly run end to end" in {
    val conf = JsonDeserializer.fromClasspathFile[StreamProcessorConf]("stream_test_config.json")
    val streamProcessor = new SensorDataStreamProcessor(spark, conf)

    streamProcessor.startStream()

    // TODO here I would first stop the streaming, by calling streamProcessor.stopStream() and then assert that
    // the stream was correctly processed by checking the generated output
  }
}