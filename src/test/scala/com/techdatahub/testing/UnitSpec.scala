package com.techdatahub.testing

import com.techdatahub.utils.Loggable
import org.scalatest.{BeforeAndAfter, FlatSpec}

abstract class UnitSpec extends FlatSpec with BeforeAndAfter with Loggable