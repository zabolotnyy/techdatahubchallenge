package com.techdatahub.testing

import java.io.File

import com.techdatahub.utils.SparkUtils
import org.apache.spark.network.util.JavaUtils
import org.apache.spark.sql.SparkSession

trait SparkTestingEnv {

  var spark: SparkSession = _

  def startSpark(withHiveSupport: Boolean = false) = {
    spark = SparkUtils.newSparkSession("test", hiveSupportEnabled = withHiveSupport, uiEnabled = false)
  }

  def shutdownSpark() = {
    if(spark != null) {
      spark.stop()
      val metastoreDir = new File("metastore_db")
      val warehouseDir = new File("spark-warehouse")
      if(metastoreDir.exists()) JavaUtils.deleteRecursively(new File("metastore_db"))
      if(warehouseDir.exists()) JavaUtils.deleteRecursively(new File("spark-warehouse"))
    }
  }
}
