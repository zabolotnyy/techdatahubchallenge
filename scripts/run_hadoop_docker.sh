#!/usr/bin/env bash

INPUT_DATA_DIR=$PWD/../data/
INPUT_DATA_CONTAINER_DIR=/usr/local/input_data

docker run  \
    --rm \
    -it \
    --name hadoop \
    --volume $INPUT_DATA_DIR:$INPUT_DATA_CONTAINER_DIR \
    -p 9000:9000 \
    -p 50070:50070 \
    sequenceiq/hadoop-docker:2.7.0 /etc/bootstrap.sh -bash