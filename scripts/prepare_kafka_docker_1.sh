#!/usr/bin/env bash

KAFKA_TOPIC="sensor-data"
KAFKA_TOPIC_PARTITIONS=4

# Define the create Kafka topic command
CREATE_TOPIC_COMMAND="\$KAFKA_HOME/bin/kafka-topics.sh \
  --create \
  --zookeeper localhost:2181 \
  --replication-factor 1 \
  --partitions $KAFKA_TOPIC_PARTITIONS \
  --topic $KAFKA_TOPIC"

# Create the topic
echo -e "\n>>> Creating the Kafka topic '$KAFKA_TOPIC'"
docker exec kafka sh -c "$CREATE_TOPIC_COMMAND"