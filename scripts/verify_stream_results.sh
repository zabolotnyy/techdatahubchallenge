#!/usr/bin/env bash

DATABASE=testdb
TABLE=stream_result

echo -e "\n\n###################################################"
echo "### Sample of the inserted to the DB records"
echo "###################################################"
docker exec -it postgres psql -h localhost -U postgres $DATABASE -c "SELECT * FROM $TABLE LIMIT 20"
