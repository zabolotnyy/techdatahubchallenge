#!/usr/bin/env bash

echo -e "\n\n#################################"
echo "### Avg Accelerations result:"
echo "#################################"

LAST_AVG_ACCEL_RESULT_FILE=`docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -ls -R /user/root/output/avg_accel_*/part* | sort -k6,7 | tail -1 | awk -F ' ' '{print $8}'`
docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -cat $LAST_AVG_ACCEL_RESULT_FILE

echo -e "\n\n########################"
echo "### Max Jerk result:"
echo "########################"

LAST_MAX_JERK_RESULT_FILE=`docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -ls -R /user/root/output/max_jerk_*/part* | sort -k6,7 | tail -1 | awk -F ' ' '{print $8}'`
docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -cat $LAST_MAX_JERK_RESULT_FILE

echo -e "\n"