#!/usr/bin/env bash

INPUT_DATA_DIR=$PWD/../data/
INPUT_DATA_CONTAINER_DIR=/usr/local/input_data

# See https://hub.docker.com/r/spotify/kafka
docker run \
    --rm \
    -it \
    --name kafka \
    --volume $INPUT_DATA_DIR:$INPUT_DATA_CONTAINER_DIR \
    -p 2181:2181 \
    -p 9092:9092 \
    --env ADVERTISED_HOST=localhost \
    --env ADVERTISED_PORT=9092 \
    spotify/kafka