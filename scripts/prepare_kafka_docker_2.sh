#!/usr/bin/env bash

INPUT_DATA_CONTAINER_DIR="/usr/local/input_data"

KAFKA_TOPIC="sensor-data"

# Define the produce to Kafka topic command
# We also attach a Key to each produced record. See http://bigdatums.net/2017/05/21/send-key-value-messages-kafka-console-producer/
# By default Kafka distributes the records with the same Key to the same Partition, by determining the HASH(key) % NUM_PARTITIONS
# If Key is not defined, records are distributed in round-robin fashion
PRODUCE_COMMAND="\$KAFKA_HOME/bin/kafka-console-producer.sh \
  --broker-list localhost:9092 \
  --property \"parse.key=true\" \
  --property \"key.separator=:\" \
  --topic $KAFKA_TOPIC"

echo -e "\n>>> Getting the data files sorted by time"
ORDERED_DATA_FILES=`docker exec kafka ls $INPUT_DATA_CONTAINER_DIR | awk -F '[_\.]' '{print $(NF-1), $0}' | sort -k1 | cut -d ' ' -f 2`

# Produce each data file to Kafka
for f in $ORDERED_DATA_FILES
do
  echo -e "\n>>> Producing $f to Kafka topic"
  FILE=$INPUT_DATA_CONTAINER_DIR/$f
  TEMP_FILE=$INPUT_DATA_CONTAINER_DIR/temp.csv

  # Strip the CSV header and append the filename to be used as a Kafka record Key, with a ":" separator
  docker exec kafka sh -c "sed '1d' $FILE | sed 's/^/$f:/' > $TEMP_FILE"

  # Produce the records from the temporary generated file
  docker exec kafka sh -c "$PRODUCE_COMMAND < $TEMP_FILE"
done;

docker exec kafka rm $TEMP_FILE