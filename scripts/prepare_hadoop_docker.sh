#!/usr/bin/env bash

# Copy all the input data files contained within the mounted volume to HDFS
docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -put /usr/local/input_data/ /user/root/

# Show the copied files on HDFS
echo "Listing copied HDFS files:"
docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -ls -R /user/root/input_data

# Since the Docker has the ACLs disabled, by setting "dfs.namenode.acls.enabled" to false,
# we just add the Write permission to Others, so that Spark can write to the output dir
docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -mkdir /user/root/output
docker exec hadoop /usr/local/hadoop/bin/hdfs dfs -chmod o+w /user/root/output