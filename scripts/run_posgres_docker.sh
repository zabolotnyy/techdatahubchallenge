#!/usr/bin/env bash

# See https://hub.docker.com/_/postgres
docker run \
    --rm \
    -it \
    --name postgres \
    -p 5432:5432 \
    -e POSTGRES_PASSWORD=mysecretpassword \
    postgres:9.6