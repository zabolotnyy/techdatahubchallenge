#!/usr/bin/env bash

DATABASE_NAME="testdb"

# Create the database
docker exec -it postgres psql -h localhost -U postgres -c "CREATE DATABASE $DATABASE_NAME"

CREATE_TABLE_STATEMENT="
CREATE TABLE stream_result (
  id            SERIAL PRIMARY KEY,
  source        VARCHAR(100) NOT NULL,
  window_from   TIMESTAMP NOT NULL,
  window_to     TIMESTAMP NOT NULL,
  window_points INTEGER NOT NULL,
  avg_accel_x   FLOAT8 NOT NULL,
  avg_accel_y   FLOAT8 NOT NULL,
  avg_accel_z   FLOAT8 NOT NULL,
  breaking      BOOL NOT NULL,
  acceleration  BOOL NOT NULL,
  right_turn    BOOL NOT NULL,
  left_turn     BOOL NOT NULL
)"

# Create the table
docker exec -it postgres psql -h localhost -U postgres $DATABASE_NAME -c "$CREATE_TABLE_STATEMENT"