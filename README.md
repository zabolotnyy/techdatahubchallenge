

### Solution Overview

The solution is a Maven project with its core implemented in Scala and Spark, interacting with HDFS, Kafka and PostgreSQL.

To make the local execution possible, Spark's local execution mode is used together with the application's configuration to connect to HDFS, Kafka and PostgreSQL running in Docker containers. This means that to run the solution in a production environment you'll only have to change the application's configurations (JSON files) and adapt the execution Bash scripts (e.g use the `spark-submit` to launch the applications on a Cluster).

#### Average Acceleration Values

Average acceleration values were simply calculated using the `AVG()` aggregation function over `GROUP BY event, label`. The result also includes a _total_accel_ column holding the Resultant Vector of the average acceleration values, calculated with `sqrt(avg_accel_x ^ 2 + avg_accel_y ^ 2 + avg_accel_z ^2)`.   

Output:

```
+--------------------+-----+-----------+--------------------+--------------------+--------------------+------------------+
|               event|label|total_count|         avg_accel_x|         avg_accel_y|         avg_accel_z|       total_accel|
+--------------------+-----+-----------+--------------------+--------------------+--------------------+------------------+
|     aggressive_bump|    0|       7299|0.013834093990090...|0.061373554952406...|0.749088198015960...|0.7517254976457973|
|     aggressive_bump|    1|       4787|-0.06585153447617...|0.032217779371239...|0.748154789582906...|0.7517379856547916|
|aggressive_longit...|    0|       7784|0.072664043637073...|0.059176602371182...|0.749447543951247...|0.7552837576966979|
|aggressive_longit...|    1|       4838|0.011803175780732...|0.063491886636606...|0.749371049114868...|0.7521485916218317|
|     aggressive_turn|    0|       6158|0.071152749148246...|0.077649637517289...|0.751434504947092...| 0.758779279595194|
|     aggressive_turn|    1|       3356|0.051852949856113...|0.041711172341479...|0.751522505613703...|0.7544631380993905|
|normal_longitudin...|    0|      13312|0.026090085983705...|0.078436031312855...|0.751427819746403...| 0.755960760809436|
|        normal_mixed|    0|      24146|0.026530769380152...|0.063760534961229...|0.752289309213179...|0.7554525083014076|
|         normal_turn|    0|       9755|0.068143501200714...|0.070012971454277...|0.751433206380543...|0.7577580198051977|
+--------------------+-----+-----------+--------------------+--------------------+--------------------+------------------+
```

#### Maximum Acceleration Jerk

To determine the maximum acceleration Jerk we calculate the _total_jerk_ for each dataset row by using its acceleration values and the acceleration values from the previous row. To get those values the table was joined with itself.

Total jerk was calculated as `abs(jerk_accel_x) + abs(jerk_accel_y) + abs(jerk_accel_z)`, where `jerk_accel_x` equals to `(accel_x - prev_accel_x) / (timestamp - prev_timestamp)`.

The intermediate generated table looks like this:

```text
+--------------------+---+-------------+--------------------+
|         source_file|row|    timestamp|          total_jerk|
+--------------------+---+-------------+--------------------+
|aggressive_bump_1...|  1|1550163148368| 2.64921039342875E-5|
|aggressive_bump_1...|  2|1550163148418|1.037954539060597E-4|
|aggressive_bump_1...|  3|1550163148468| 2.90443003177658E-5|
|aggressive_bump_1...|  4|1550163148518|   7.021427154542E-6|
|aggressive_bump_1...|  5|1550163148568|  5.4929107427597E-5|
|aggressive_bump_1...|  6|1550163148618| 5.39705157279972E-5|
|aggressive_bump_1...|  7|1550163148668|  5.4929107427597E-5|
|aggressive_bump_1...|  8|1550163148718| 2.93955206871032E-5|
|aggressive_bump_1...|  9|1550163148768| 5.93983381986607E-5|
|aggressive_bump_1...| 10|1550163148818|1.022295653820038E-4|
|aggressive_bump_1...| 11|1550163148868| 6.29092007875427E-5|
|aggressive_bump_1...| 12|1550163148918| 5.04598021507274E-5|
|aggressive_bump_1...| 13|1550163148968|5.202569067478128E-5|
|aggressive_bump_1...| 14|1550163149018|  5.4929107427597E-5|
|aggressive_bump_1...| 15|1550163149068|1.137202233076096...|
|aggressive_bump_1...| 16|1550163149118| 3.99278104305272E-5|
|aggressive_bump_1...| 17|1550163149168|1.102095842361456E-4|
|aggressive_bump_1...| 18|1550163149218| 5.49291819334031E-5|
|aggressive_bump_1...| 19|1550163149268|1.337981969118113E-4|
|aggressive_bump_1...| 20|1550163149318|1.002846658229816E-4|
+--------------------+---+-------------+--------------------+
```

Then the row with the greater `total_jerk` value is picked, producing the required output: 

```text
+--------------------+-----+-------------+--------------------+
|         source_file|  row|    timestamp|      formatted_time|
+--------------------+-----+-------------+--------------------+
|aggressive_bump_1...|10038|1550163650268|2019-02-14 17:00:...|
+--------------------+-----+-------------+--------------------+
```

#### Binary Discrimination;

To process the continuous stream of data and binary discriminate the object movements by analyzing the accelerometer data Spark's Structured Streaming engine was used.
 
The implemented Structured Streaming solution processes the stream of data from Kafka and for each source (e.g aggressive_turn_1549625320507325.csv) assigns its records to tumbling windows of 5 seconds (can be set to any value). Then it calculates the average acceleration values for each axis and compares them to configured threshold values, which decides if the movement was a break / acceleration or left / right.

For example with a window of 5 seconds 100 sensor records can be considered to calculate the average acceleration values, since the sensor generates a record each 50ms.

The results with a classification for each window of 5 seconds are written to a PostgreSQL database. 
 

### System Requirements
To run the solution you'll need an environment with the following components:
- Bash shell
- Maven
- Java
- Docker

### Build

The solution can be built by running the `mvn clean package` command at the root of the project, where `pom.xml` resides.

When build completes, navigate to the *target* directory and unzip the generated *challenge-\<version>.zip* file.

```bash
unzip -d challenge challenge-1.0.0.zip
```

This ZIP contains the files below:

```bash
[andriymz@fedora27 challenge]$ tree
.
├── configs
│   ├── batch_config.json
│   ├── log4j.properties
│   └── stream_config.json
├── hadoop-configs
│   └── core-site.xml
├── jars
│   └── challenge-1.0.0.jar
└── scripts
    ├── run_batch_processor.sh
    └── run_stream_processor.sh

4 directories, 7 files
```

### Run

Project's root contains `data` and `script` directories required to run the solution.

```bash
[andriymz@fedora27 TechHubChallenge]$ tree data
data
├── aggressive_bump_1550163148318484.csv
├── aggressive_longitudinal_acceleration_1549653321089461.csv
├── aggressive_turn_1549625320507325.csv
├── normal_longitudinal_acceleration_1549908723215048.csv
├── normal_mixed_1549901031015048.csv
├── normal_mixed_1550054269957615.csv
└── normal_turn_1549626293857325.csv

0 directories, 7 files
```

```bash
[andriymz@fedora27 TechHubChallenge]$ tree scripts/
scripts/
├── prepare_hadoop_docker.sh
├── prepare_kafka_docker_1.sh
├── prepare_kafka_docker_2.sh
├── prepare_postgres_docker.sh
├── run_hadoop_docker.sh
├── run_kafka_docker.sh
├── run_posgres_docker.sh
├── verify_batch_results.sh
└── verify_stream_results.sh

0 directories, 9 files
```

#### Batch solution

To run the batch solution follow the steps below:
1. Execute the `run_hadoop_docker.sh` script and wait until the Docker starts;
2. Execute the `prepare_hadoop_docker.sh` script to copy all the input data files to HDFS;
3. Execute the `run_batch_processor.sh` script to run the application;
4. Execute the `verify_batch_results.sh` script to check the results.

#### Streaming solution

To run the streaming solution follow the steps below:
1. Execute the `run_kafka_docker.sh` script and wait until the Docker starts;
2. Execute the `run_postgres_docker.sh` script and wait until the Docker starts;
3. Execute the `prepare_kafka_docker_1.sh` script to create the Kafka topic;
4. Execute the `prepare_postgres_docker.sh` script to create the database & table;
5. Execute the `run_stream_processor.sh` script in a separate shell to start the long-running streaming application;
6. Execute the `prepare_kafka_docker_2.sh` script to produce all the input data to the Kafka topic;
7. Execute the `verify_stream_results.sh` script to check the results while the streaming application is running. Feel free to change the SQL query within the script;
8. Terminate the application launched in a separate shell.

### Results

You can use the `verify_batch_results.sh` and `verify_stream_results.sh` to check the execution results.

```bash
[andriymz@fedora27 scripts]$ ./verify_batch_results.sh 

#################################
### Avg Accelerations result:
#################################
event,label,total_count,avg_accel_x,avg_accel_y,avg_accel_z,total_accel
aggressive_bump,0,7299,0.013834093990090074075750,0.061373554952406281845705,0.749088198015960639293054,0.7517254976457973
aggressive_bump,1,4787,-0.065851534476172212668394,0.032217779371239389939942,0.748154789582906547963234,0.7517379856547916
aggressive_longitudinal_acceleration,0,7784,0.072664043637073513592138,0.059176602371182136820028,0.749447543951247235868448,0.7552837576966979
aggressive_longitudinal_acceleration,1,4838,0.011803175780732849075114,0.063491886636606483489334,0.749371049114868334084332,0.7521485916218317
aggressive_turn,0,6158,0.071152749148246775830935,0.077649637517289717804449,0.751434504947092438746346,0.758779279595194
aggressive_turn,1,3356,0.051852949856113335893623,0.041711172341479819022110,0.751522505613703266686532,0.7544631380993905
normal_longitudinal_acceleration,0,13312,0.026090085983705298927727,0.078436031312855028255837,0.751427819746403165241887,0.755960760809436
normal_mixed,0,24146,0.026530769380152431618918,0.063760534961229786594624,0.752289309213179781094177,0.7554525083014076
normal_turn,0,9755,0.068143501200714171471871,0.070012971454277073282911,0.751433206380543982788314,0.7577580198051977

########################
### Max Jerk result:
########################
source_file,row,timestamp,formatted_time
aggressive_bump_1550163148318484.csv,10038,1550163650268,2019-02-14 17:00:50.268
```

```bash
[andriymz@fedora27 scripts]$ ./verify_stream_results.sh 

###################################################
### Sample of the inserted to the DB records
###################################################
 id |                source                |     window_from     |      window_to      | window_points |    avg_accel_x     |     avg_accel_y     |    avg_accel_z    | breaking | acceleration | right_turn | left
_turn 
----+--------------------------------------+---------------------+---------------------+---------------+--------------------+---------------------+-------------------+----------+--------------+------------+-----
------
  1 | normal_turn_1549626293857325.csv     | 2019-02-08 11:51:55 | 2019-02-08 11:52:00 |           100 | 0.0110460705973674 | -0.0960069790959824 | 0.751733364462852 | t        | f            | t          | f
  2 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:29:45 | 2019-02-08 11:29:50 |           100 | 0.0867963590286672 | -0.0191164210950956 | 0.750839897990227 | f        | t            | t          | f
  3 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:32:55 | 2019-02-08 11:33:00 |           100 | 0.0657782504177885 |   0.121789734819904 | 0.750978573560715 | f        | t            | f          | t
  4 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:31:40 | 2019-02-08 11:31:45 |           100 | 0.0789225895237178 |  0.0625196773279458 | 0.751187454462051 | f        | t            | f          | t
  5 | normal_turn_1549626293857325.csv     | 2019-02-08 11:51:40 | 2019-02-08 11:51:45 |           100 | 0.0814748159795999 |  0.0524848591035698 | 0.751601712703705 | f        | t            | f          | f
  6 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:30:30 | 2019-02-08 11:30:35 |           100 | 0.0809247587621212 |  0.0372277934569866 | 0.751566604971886 | f        | t            | f          | f
  7 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:32:00 | 2019-02-08 11:32:05 |           100 |  0.096663112025708 |   0.125279481858015 | 0.751101441979408 | f        | t            | f          | t
  8 | normal_turn_1549626293857325.csv     | 2019-02-08 11:51:10 | 2019-02-08 11:51:15 |           100 | 0.0657785185612738 |   0.107456309730187 | 0.751391073465347 | f        | t            | f          | t
  9 | normal_turn_1549626293857325.csv     | 2019-02-08 11:48:30 | 2019-02-08 11:48:35 |           100 | 0.0739311704039574 |  0.0800758399441838 | 0.751362986564636 | f        | t            | f          | t
 10 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:35:05 | 2019-02-08 11:35:10 |           100 | 0.0163727988890605 |  0.0803880115784705 | 0.751501658558846 | t        | f            | f          | t
 11 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:32:25 | 2019-02-08 11:32:30 |           100 | 0.0421931786322966 |  0.0327467290312052 | 0.752261717319489 | f        | f            | f          | f
 12 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:31:00 | 2019-02-08 11:31:05 |           100 | 0.0883389905374497 | 0.00114582287962548 | 0.751154102683067 | f        | t            | t          | f
 13 | normal_turn_1549626293857325.csv     | 2019-02-08 11:50:40 | 2019-02-08 11:50:45 |           100 | 0.0748619176633656 |  0.0569950803893153 | 0.751326130032539 | f        | t            | f          | f
 14 | normal_turn_1549626293857325.csv     | 2019-02-08 11:48:35 | 2019-02-08 11:48:40 |           100 | 0.0902766928449273 |  0.0920632256695535 |  0.75118744969368 | f        | t            | f          | t
 15 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:32:40 | 2019-02-08 11:32:45 |           100 | 0.0808198457956314 | -0.0271048797294497 | 0.751536765694618 | f        | t            | t          | f
 16 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:29:05 | 2019-02-08 11:29:10 |           100 | 0.0179153349000262 |  0.0832326076272875 | 0.752575929760933 | t        | f            | f          | t
 17 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:31:05 | 2019-02-08 11:31:10 |           100 |  0.073555205585435 |  0.0624308207258582 | 0.751192722916603 | f        | t            | f          | t
 18 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:30:35 | 2019-02-08 11:30:40 |           100 | 0.0444517768593505 |  0.0796179436333477 | 0.752082683444023 | f        | f            | f          | t
 19 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:30:00 | 2019-02-08 11:30:05 |           100 | 0.0387120397854596 |  0.0436176366813015 | 0.751698256731033 | f        | f            | f          | f
 20 | aggressive_turn_1549625320507325.csv | 2019-02-08 11:31:45 | 2019-02-08 11:31:50 |           100 | 0.0514732904848643 |  0.0504450697894208 | 0.752200281620026 | f        | f            | f          | f
(20 rows)
```

### Configurations

The applications are configured with a JSON file:

```json
[andriymz@fedora27 configs]$ cat batch_config.json 
{
  "inputDataDir" : "hdfs://localhost:9000/user/root/input_data",
  "outputDir" : "hdfs://localhost:9000/user/root/output"
}
```

```json
[andriymz@fedora27 configs]$ cat stream_config.json 
{
  "kafka": {
    "kafkaTopic": "sensor-data",
    "kafkaBootstrapServers": "localhost:9092"
  },
  "accelerationBounds": {
    "breakUpperBound": 0.02,
    "accelerationLowerBound": 0.06,
    "rightUpperBound": 0.02,
    "leftLowerBound": 0.06
  },
  "jdbc": {
    "dbType": "postgres",
    "url": "jdbc:postgresql://localhost:5432/testdb",
    "user": "postgres",
    "password": "mysecretpassword",
    "outputTable": "stream_result"
  }
}
```


